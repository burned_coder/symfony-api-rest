<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Alumnos;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;


class AlumnosController extends AbstractController
{
    #[Route('/alumnos', name: 'app_alumnos')]
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/AlumnosController.php',
        ]);
    }
    

    
    public function getAllAlum(ManagerRegistry $doctrine): JsonResponse
    {
        $alumnos_repo = $doctrine->getRepository(Alumnos::class);
        
        $alumnos = $alumnos_repo->findAll();

        $datos = [];

        foreach($alumnos as $alumno){
            $alumAr = [
                'id' => $alumno->getId(),
                'dni' => $alumno->getDni(),
                'nombre' => $alumno->getNombre(),
                'apellidos' => $alumno->getApellidos(),
                'nota_media' => $alumno->getNotaMedia()
            ];

            array_push($datos, $alumAr);
        }
        
        return $this->json(
            $datos
        );
    }

    public function getAlum(ManagerRegistry $doctrine, $id): JsonResponse
    {
        $alumnos_repo = $doctrine->getRepository(Alumnos::class);
        
        $alumnos = $alumnos_repo->find($id);

        $data = [
            'id' => $alumnos->getId(),
            'dni' => $alumnos->getDni(),
            'nombre' => $alumnos->getNombre(),
            'apellidos' => $alumnos->getApellidos(),
            'nota_media' => $alumnos->getNotaMedia()
        ];
        
        return $this->json([
            $data
        ]);
    }

    public function postAlum(ManagerRegistry $doctrine, Request $req): JsonResponse
    {
        $em = $doctrine->getManager();               

       
        $datos = str_replace('%20', ' ', $req->getcontent());
        $datos = explode('&', $datos);

        $allData = [];
        
        foreach($datos as $dato){
            array_push($allData, explode("=", $dato)[1]);
        }      
        
        $alumno = new Alumnos();        

        $alumno -> setDni($allData[0]);
        $alumno -> setNombre($allData[1]);
        $alumno -> setApellidos($allData[2]);
        $alumno -> setNotaMedia((int)$allData[3]);

        $em->persist($alumno);
        $em->flush();
        
        return $this->json([
            "Los datos han sido introducidos correctamente"
        ]);
    }

    public function updateAlum(ManagerRegistry $doctrine, $id, Request $req): JsonResponse
    {   
        
        $em = $doctrine->getManager();  
        $alumnoRepo = $doctrine->getRepository(Alumnos::class);
        
        $datos = str_replace('%20', ' ', $req->getcontent());
        $datos = explode('&', $datos);

        $allData = [];
        
        foreach($datos as $dato){
            array_push($allData, explode("=", $dato)[1]);
        } 
       
        $alumno = $alumnoRepo->find($id);      

        if(!$alumno){
            $mensaje = "No existe un alumno con esa id en la BBDD";
        }else{
            if($allData[0] != ""){
                $alumno -> setDni($allData[0]);
            }

            if($allData[1] != ""){
                $alumno -> setNombre($allData[1]);
            }
            
            if($allData[2] != ""){
                $alumno -> setApellidos($allData[2]);
            }

            if($allData[3] != "" && (int)$allData[3] > 0){
                $alumno -> setNotaMedia((int)$allData[3]);
            }

            $em->persist($alumno);
            $em->flush();

            $mensaje = 'Datos del alumno modificados con éxito';
        }
        
        
        return $this->json([
            $mensaje
        ]);
    }

    public function deleteAlum(ManagerRegistry $doctrine, $id): JsonResponse
    {
        $alumnos_repo = $doctrine->getRepository(Alumnos::class);
        $em = $doctrine->getManager();
        
        $alumno = $alumnos_repo->find($id);

        if($alumno){
            $em->remove($alumno);
            $em->flush();
            $mensaje = "Alumno borrado correctamente";
        }else{
            $mensaje = "Alumno no encontrado en la BBDD";
        }        
        
        return $this->json([
            $mensaje
        ]);
    }

    
    
}
